function salutLesTerriens(longueur, largeur, hauteur){

    if(isset(longueur && largeur && hauteur) && longueur>0 && largeur>0 && hauteur>0) {
        let longueur = document.getElementById('longueur').value;
        let largeur = document.getElementById('largeur').value;
        let hauteur = document.getElementById('hauteur').value;

        let result = {};
        result.longueur = is_numeric(longueur);
        result.largeur = largeur;
        result.hauteur = hauteur;

        calculer(result);
    }else
        alert "Il manque des données";
}

function calculer(result){
    let pente = Math.sqrt((Math.pow(result.hauteur, 2) + (Math.pow(result.largeur/2), 2)));
    let surface = Math.round(pente * result.longueur) * 2;
    alert(surface);

}